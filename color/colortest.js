// experiment conditions
// declaration of independent variables
var minimumAge = 10, maximumAge = 90;

var totalColorCount = 6;
var totalTestCount = 4;
var maxDelay = 10000;
var minDelay = 5000;

// colors
var testColors = ["#FF0000", "#FFFF00", "#FFA500", "#800080", "#008000", "#0000FF"];

// results
var results = {
    age: null,
    gender: null,
    date: null,
    version: 1,
    reactionTime: [ [], [], [], [] ]
};

// indicator for readiness for key press
var waitingForInput = false;

// randomizes sequence of colors for each test
var colorSequence = shuffleEachTest(createColorSequence());

// indicator for the test being started
var testStarted = false;

// variables that count tests (trials) and colors
var trialCount = 0, testCount = 0;

// variables, which signify reaction time
var startTime = 0, endTime = 0, delay = 0;

// indicator for the end of the test
var endTest = false;

// create initial color sequence
function createColorSequence() {
    var i, c, sequence = [];
    for (i = 0; i < totalTestCount; i++) {
        sequence[i] = [];
        for (c = 0; c < totalColorCount; c++) {
            sequence[i][c] = c;
        }
    };
    return sequence;
}

// execute shuffle for each trial
function shuffleEachTest(array2D) {
    var y = array2D.length;
    while (y) {
        y--;
        array2D[y] = shuffle(array2D[y]);
    } 
    return array2D;
}

// randomizes color sequence
function shuffle(array) {
    var m = array.length, t, i;
    while (m) {
       i = Math.floor(Math.random()*m);
        m--;
        
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    } 
    return array;
}

// function to populate age selector
function populateAges() {
    var x = document.getElementById("ageSelector");
    for (var i = minimumAge-1; i <= maximumAge; i++) {
        var option = document.createElement("option");
        if (i >= minimumAge) {
            option.text = i.toString();
        }
        x.add(option);
    }
}

// check that age and gender are selected & launch start screen
function validateInput() {
    var e = document.getElementById("ageSelector");
    var ageInput = e.options[e.selectedIndex].text;
    
    var genderInput = null;
    if (document.getElementById("male").checked) {
        genderInput="male"
    } else if (document.getElementById("female").checked) {
        genderInput="female"
    } 
    
    if (!ageInput || !genderInput) {
        alert("please fill in your age and gender")
    } else { 
        results.age=ageInput;
        results.gender=genderInput;
        startScreen();
    };
}
 
// function for start screen
function startScreen() {
    document.getElementById("introDiv").style.display="none";
    document.getElementById("startDiv").style.display="block";
    addEventHandler();
    waitingForInput = true;
}

// adds keypress event handler
function addEventHandler() {
    if (document.addEventListener) {
        document.addEventListener("keydown",keydownHandler, false);
    } else if (document.attachEvent) {
        document.attachEvent("onkeydown",keydownHandler);   
    }
}

// removes keypress event handler
function removeEventHandler() {
    if (document.removeEventListener) {
        document.removeEventListener("keydown",keydownHandler);
    } else if (document.detachEvent) {
        document.detachEvent("onkeydown",keydownHandler);   
    }
}

// if spacebar is pressed and we are waiting for input, launch next step
function keydownHandler(e) {
    if (e.keyCode == 32 && waitingForInput) {
        nextStep();
    }
}

// switches to next color
function nextStep() {
    
    waitingForInput = false;
    
    if (!testStarted) {
        document.getElementById("startDiv").style.display="none";
        document.getElementById("colorDiv").style.display="block";
        testStarted = true;
    } else {
        endTime = Date.now();
        delay = endTime - startTime;
        var shownColor = colorSequence[trialCount][testCount];
        results.reactionTime[trialCount][shownColor] = delay;
        
        // changes trialCount and testCount
        if (testCount < totalColorCount - 1) {
            testCount++;
        } else {
            if (trialCount < totalTestCount - 1) {
                trialCount++;
                testCount = 0;
            } else {
                endTest = true;
            }
        }
    };
    
    resetColor();
    
    if (!endTest) {
        var random = randomDelay();
        setTimeout(setColor,random);
    } else {
        removeEventHandler();
        console.log(results);
        saveResults();
        showResults();
    }
}

// generates random delay time
function randomDelay() {
    return Math.round(Math.random()*(maxDelay-minDelay))+minDelay;
}

// switches color to white
function resetColor() {
    document.getElementById("colorDiv").style.backgroundColor="white";
}

// changes colors
function setColor() {
    var nextColor = colorSequence[trialCount][testCount];
    document.getElementById("colorDiv").style.backgroundColor=testColors[nextColor];
    startTime = Date.now();
    waitingForInput = true;
}

// saves results into spreadsheet
function saveResults() {
    var data = {
        "entry.1432890639":     "v1test",
        "entry.1450709626":     navigator.userAgent,
        "entry.1075319523":     results.age,
        "entry.1709829501":     results.gender,
        "entry.1230667962":     results.reactionTime[0][0],
        "entry.192374492":      results.reactionTime[0][1],
        "entry.1971174884":     results.reactionTime[0][2],
        "entry.784191020":      results.reactionTime[0][3],
        "entry.335488924":      results.reactionTime[0][4],
        "entry.91123537":       results.reactionTime[0][5],
        "entry.822182552":      results.reactionTime[1][0],
        "entry.1504750321":     results.reactionTime[1][1],
        "entry.529303227":      results.reactionTime[1][2],
        "entry.13631963":       results.reactionTime[1][3],
        "entry.461880235":      results.reactionTime[1][4],
        "entry.1223698650":     results.reactionTime[1][5],
        "entry.1726097086":     results.reactionTime[2][0],
        "entry.116975268":      results.reactionTime[2][1],
        "entry.155478122":      results.reactionTime[2][2],
        "entry.2016589306":     results.reactionTime[2][3],
        "entry.1001362913":     results.reactionTime[2][4],
        "entry.1576971978":     results.reactionTime[2][5],
        "entry.2011205652":     results.reactionTime[3][0],
        "entry.428839265":      results.reactionTime[3][1],
        "entry.716208625":      results.reactionTime[3][2],
        "entry.801449398":      results.reactionTime[3][3],
        "entry.261530153":      results.reactionTime[3][4],
        "entry.1652499525":     results.reactionTime[3][5]
    }
    var ajaxRequest = {
        url: "https://docs.google.com/forms/d/1Rp6fTyS2Pdj2QiYbbZOjAUITMGUqpkc8fB1CxZDu8JA/formResponse?embedded=true",
        data: data,
        type: "POST",
        dataType: "jsonp",
        statusCode: {   
            0: function() {
                console.log("status code 0");
                },
            200: function() {
                console.log("status code 200");
                }
        }

    }
    $.ajax(ajaxRequest);
};

// shows results
function showResults() {
    document.getElementById("colorDiv").style.display="none";
    document.getElementById("resultsDiv").style.display="block";
    displayTable();
}

function displayTable() {
    var resultsTable = document.getElementById("resultsTable");
    var tableBody = document.createElement("TBODY");
    resultsTable.appendChild(tableBody);
    var i, m, d, newCell, row;
    
    // header row
    var header = document.createElement("TR");
    tableBody.appendChild(header);
    
    for (i=0; i < totalTestCount + 1; i++) {
        newCell = document.createElement("TH");
        header.appendChild(newCell);
        
        if (i!=0) {
            newCell.appendChild(document.createTextNode("#"+i));
        }
    } 
    
    // cells
    for (m=0; m < totalColorCount; m++) {
        row = document.createElement("TR");
        tableBody.appendChild(row);
        newCell = document.createElement("TD");
        newCell.style.color = testColors[m];
        newCell.style.borderColor = "#838b8b";
        row.appendChild(newCell);
        newCell.appendChild(document.createTextNode("\u25B6"));
    
        for (d=0; d < totalTestCount;d++) {
            newCell = document.createElement("TD");
            row.appendChild(newCell);
            newCell.appendChild(document.createTextNode(results.reactionTime[d][m]));
        }
    }
}